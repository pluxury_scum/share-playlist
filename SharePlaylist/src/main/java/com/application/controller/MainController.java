package com.application.controller;

import com.application.domain.Playlist;
import com.application.domain.User;
import com.application.repos.PlaylistRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@org.springframework.stereotype.Controller
public class MainController {

    @Autowired
    private PlaylistRepo playlistRepo;

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(@RequestParam(required = false, defaultValue = "") String filter, Model model) {
        Iterable<Playlist> playlists = playlistRepo.findAll();

        if (filter != null && !filter.isEmpty())
            playlists = playlistRepo.findByAuthor(filter);
        else
            playlists = playlistRepo.findAll();

        model.addAttribute("playlists", playlists);
        model.addAttribute("filter", filter);
        return "main";
    }

    @PostMapping("/main")
    public String add(
            @AuthenticationPrincipal User user,
            @RequestParam String album,
            @RequestParam String author,
            @RequestParam String link,
            @RequestParam String releaseYear,
            @RequestParam String comment,
            Map<String, Object> model,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        Playlist playlist = new Playlist(album, author, releaseYear, link, comment, user);

        if (file != null && !Objects.requireNonNull(file.getOriginalFilename()).isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists())
                uploadDir.mkdir();
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + resultFilename));
            playlist.setFilename(resultFilename);
        }

        playlistRepo.save(playlist);
        Iterable<Playlist> playlists = playlistRepo.findAll();
        model.put("playlists", playlists);
        return "main";
    }
}