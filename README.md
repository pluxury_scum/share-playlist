# SharePlaylist

## Table of contents
* [Description](#description)
* [Before starting](#before-starting)
* [Usage](#usage)
* [Illustrations](#illustrations)
* [Technologies](#shareplaylist-libraries-and-technologies)
* [License](#license)

## Description
SharePlaylist is a tool for sharing your favourite music playlists

## Before starting
Complete /src/main/resources/application.properties file with your upload path and database properties 

## Usage
1. Go to "/registration" directory to sign in
2. Go to "/login" directory to log in
3. Click on "Add new playlist" button to share a new playlist
4. Enter album, author, release year, comment and give a link to the playlist (do not forget http(s):// in the link folder)
5. Choose cover for the album and click on Submit button
6. Enjoy others' playlists! 

## Illustrations
[example1](https://gitlab.com/9035719268/shareplaylist/-/blob/master/illustrations/frontPage.png)

## SharePlaylist libraries and technologies
- Apache Maven
- Spring Boot
- Hibernate
- MySQL
- Freemarker
- Bootstrap
- HTML
- CSS


## License
[MIT](https://choosealicense.com/licenses/mit/)